//package com.javabrains.springlogin;


import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.client.HttpClientErrorException;
import org.junit.jupiter.api.Assertions;

import java.util.List;


//
// @SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
/**

    @Autowired
    private TestRestTemplate restTemplate;
    private ComputerModelRepository computerModelRepository;
    @LocalServerPort
    private int port;
    private String getRootUrl() {
        return "http://localhost:" + port;
    }
    @Test
    void contextLoads() {
    }
    @Test
    public void testGetAllComputers() {
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);

        ResponseEntity<String> response = restTemplate.exchange(getRootUrl() + "/getData",
                HttpMethod.GET, entity, String.class);

        Assertions.assertNotNull(response.getBody());
    }
    @Test
    public void testCreateComputer(){
        ComputerModel computer= new ComputerModel();
        computer.setId(3);
        computer.setOwner("Shachar");
        computer.setIp("123.123.123.123");
        computerModelRepository.save(computer);
        Assertions.assertNotNull(computerModelRepository.findById(3).get());
    }
    @Test
    public void testCreateUser() {
        ComputerModel computer = new ComputerModel();
        computer.setOwner("shachar");
        computer.setIp("123.234.345.455");
        ResponseEntity<ComputerModel> postResponse = restTemplate.postForEntity(getRootUrl() + "/users", computer, ComputerModel.class);
        Assertions.assertNotNull(postResponse);
        Assertions.assertNotNull(postResponse.getBody());
    }
    @Test
    public void testDeletePost() {
        int id = 2;
        ComputerModel computerModel = restTemplate.getForObject(getRootUrl() + "/api/" + id, ComputerModel.class);
        Assertions.assertNotNull(computerModel);

        restTemplate.delete(getRootUrl() + "/api/" + id);

        try {
            computerModel = restTemplate.getForObject(getRootUrl() + "/api/" + id, ComputerModel.class);
        } catch (final HttpClientErrorException e) {
            Assertions.assertEquals(e.getStatusCode(), HttpStatus.NOT_FOUND);
        }
    }



}
 **/
