package com.javabrains.springlogin;

import com.javabrains.springlogin.ComputerModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class ComputerService  {

    @Autowired
    private ComputerModelRepository computerModelRepository;

    public Optional<ComputerModel> find(Integer Id){
        return computerModelRepository.findById(Id);
    }

    public void save(ComputerModel computer){
        computerModelRepository.save(computer);
    }

    public List<ComputerModel> listAll(){
        return computerModelRepository.findAll();
    }

    public void delete(Integer Id){ computerModelRepository.deleteById(Id);}

    public ComputerModel create(ComputerModel computer) { return computerModelRepository.save(computer);   }

    public ComputerModel update(Integer id, ComputerModel computerDetails) {
        ComputerModel computer = computerModelRepository.getById(id);
        computer.setIp(computerDetails.getIp());
        computer.setOwner(computerDetails.getOwner());
        return computerModelRepository.save(computer);
    }


}
