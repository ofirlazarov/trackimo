package com.javabrains.springlogin;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import java.util.Optional;

//By extending from the Spring JpaRepository, we have some basic methods for our data repository implemented
//such as  Create new computer, Read, Update existing computers, Delete
public interface ComputerModelRepository extends JpaRepository<ComputerModel, Integer> {



}