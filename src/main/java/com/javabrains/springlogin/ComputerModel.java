package com.javabrains.springlogin;

import javax.persistence.*;
import javax.persistence.Entity;

@Entity // This tells Hibernate to make a table out of this class
@Table(name="computers")
public class ComputerModel
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    Integer id;

    @Column(name = "owner")
    String owner;

    @Column(name = "ip")
    String ip;

    public ComputerModel(){
    }

    public ComputerModel(Integer id, String owner, String ip){
        this.id = id;
        this.owner=owner;
        this.ip=ip;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }
}
