package com.javabrains.springlogin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.persistence.EntityNotFoundException;
import javax.websocket.PongMessage;
import java.net.InetAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

@CrossOrigin
@RestController // indicates that the data returned by each method will be written straight into the response body instead of rendering a template
@RequestMapping("/api")
public class ComputerRestController {

    @Autowired
    ComputerService computerService;

    @GetMapping("/get")
    public List<ComputerModel> list() {
        return computerService.listAll();
    }

    @GetMapping("/get/{id}")
    public Object read(@PathVariable Integer id) {
        Optional<ComputerModel> foundComputer = computerService.find(id);
        if (foundComputer == null) {
            return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.ok(foundComputer);
        }
    }
    @PostMapping("/create")
    public Object create(@RequestBody ComputerModel computer)
            throws URISyntaxException {
        ComputerModel createdComputer = computerService.create(computer);
        if (createdComputer == null) {
            return ResponseEntity.notFound().build();
        } else {
            URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
                    .path("/{id}")
                    .buildAndExpand(createdComputer.getId())
                    .toUri();
            return ResponseEntity.created(uri)
                    .body(createdComputer);
        }
    }

    @PutMapping("/update/{id}")
    public Object update(@RequestBody ComputerModel computerDetails, @PathVariable Integer id) {
        try{
            Optional<ComputerModel> existComputer= computerService.find(id);
            if (existComputer != null) {
                existComputer = Optional.ofNullable(computerService.update(id, computerDetails));
                return ResponseEntity.ok(existComputer);
            }
        }
        catch(EntityNotFoundException e){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.noContent().build();

    }

    @DeleteMapping("/delete/{id}")
    public Object delete(@PathVariable Integer id) {
        try{
            Optional<ComputerModel> existComputer = computerService.find(id);
            if (existComputer != null) {
                computerService.delete(id);
                return ResponseEntity.ok(existComputer);
            }
        }
        catch(EmptyResultDataAccessException e){
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.noContent().build();
    }

    @RequestMapping(value = "/ping/{id}", method = RequestMethod.POST)
    public Object ping(@PathVariable Integer id) {
        try{
            Optional<ComputerModel> existComputer= computerService.find(id);
            if(existComputer!=null){
                String ipAddress = existComputer.get().ip;
                InetAddress address = InetAddress.getByName(ipAddress);
                boolean reachable = address.isReachable(10000);
                return reachable;
            }
        } catch (Exception e){
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.noContent().build();
    }

}
