const getComputersData = async () => {
    try {
        const response = await axios.get('http://localhost:8080/api/get',{
            headers: {'Content-Type': 'application/json'}
        });
        return response.data;
    } catch (error) {
        console.error('error: '+error.message);
    }
}

const getComputerDataById = async (id) => {
    try {
        const response = await axios.get(`http://localhost:8080/api/get/${id}`,{
            headers: {'Content-Type': 'application/json'}
        });
        return response.data;
    } catch (error) {
        console.error('error: '+error.message);
    }
}

const addComputer = async (owner, ip) => {
    try {
        await axios({
            method: 'post',
            url: 'http://localhost:8080/api/create',
            headers: { 'Content-Type': 'application/json' },
            data: {
                owner: `${owner}`,
                ip: `${ip}`
            }
        });
        console.log(`Add ${owner}, ${ip}`)
    } catch (error) {
        console.log(error.message);
        alert('This IP address is already exist! Please try different IP')
    }
}

const updateComputer = async (id, owner, ip) => {
    try {
        await axios({
            method: 'put',
            url: `http://localhost:8080/api/update/${id}`,
            headers: { 'Content-Type': 'application/json' },
            data: {
                owner: `${owner}`,
                ip: `${ip}`
            }
        });
        console.log(`Update ${owner}, ${ip}`)
    } catch (error) {
        console.log(error.message);
    }
}

const deleteComputer = async (id) => {
    try {
        await axios.delete(`http://localhost:8080/api/delete/${id}`,{
            headers: { 'Content-Type': 'application/json' }
        })

        console.log(`Delete ${id}`)
    } catch (error) {
        console.log(error.message);
    }
}

const pingComputer = async (id) => {
    try {
        const response = await axios({
            method: 'post',
            url: `http://localhost:8080/api/ping/${id}`,
            headers: { 'Content-Type': 'application/json' },
        });
        if(response.data){
            alert('The computer is reachable')
        }
        else{
            alert('The computer is not reachable or the IP Address is not vaild')
        }
        console.log(`Ping ${id}`)
    } catch (error) {
        console.log(error.message);
    }
}

const createEditSection=(id,owner,ip)=>{
    const ownerInputElement=document.createElement('input')
    const ipInputElement=document.createElement('input')
    ownerInputElement.value=owner;
    ipInputElement.value=ip;
    const saveButton=document.createElement('button');
    saveButton.innerHTML='Save';
    saveButton.addEventListener('click',async()=>{
        if (ownerInputElement.value && ipInputElement.value)
            await updateComputer(id,ownerInputElement.value,ipInputElement.value);
        await renderComputers();
    })
    const editContainerElement=document.createElement('div');
    editContainerElement.appendChild(ownerInputElement);
    editContainerElement.appendChild(ipInputElement);
    editContainerElement.appendChild(saveButton);
    return editContainerElement;
}

const addEventsToComputerItem = (computerItemElement, id) => {
    const pingButton = computerItemElement.getElementsByClassName("ping-button")[0]
    const editButton = computerItemElement.getElementsByClassName("edit-button")[0]
    const deleteButton = computerItemElement.getElementsByClassName("delete-button")[0]
    editButton.addEventListener('click', async ()=>{
        console.log(editButton.parentElement.parentElement)
        const computerContentSection= editButton.parentElement.parentElement.getElementsByClassName('computer-content__item')[0];
        const ownerText = computerContentSection.querySelectorAll('p')[0].innerHTML;
        const ipText = computerContentSection.querySelectorAll('p')[1].innerHTML;
        const editSection=createEditSection(id,ownerText,ipText)
        editButton.parentElement.parentElement.replaceChild(editSection,computerContentSection);
    });

    deleteButton.addEventListener('click', async() => {
        await deleteComputer(id)
        await renderComputers()});
    pingButton.addEventListener('click', async() => {
        await pingComputer(id)
    });
}

//DOM
const appendNewComputerRow = (computerDetailes) => {
    //item detailes content
    const computerDetailesContent = document.createElement('div');
    computerDetailesContent.classList.add("computer-content__item");

    const computerOwnerElement = document.createElement('p');
    computerOwnerElement.innerHTML = computerDetailes.owner;
    const computerIpElement = document.createElement('p');
    computerIpElement.innerHTML = computerDetailes.ip;

    computerDetailesContent.appendChild(computerOwnerElement)
    computerDetailesContent.appendChild(computerIpElement)

    //item buttons
    const computerDetailesButtons = document.createElement('div');
    computerDetailesButtons.classList.add("computer-buttons__item");

    const computerPingElement = document.createElement('button');
    computerPingElement.classList.add("ping-button")
    computerPingElement.innerHTML = "Ping";
    const computerDeleteElement = document.createElement('button');
    computerDeleteElement.classList.add("delete-button")
    computerDeleteElement.innerHTML = "Delete";
    const computerEditElement = document.createElement('button');
    computerEditElement.classList.add("edit-button")
    computerEditElement.innerHTML = "Edit";
    computerDetailesButtons.appendChild(computerPingElement)
    computerDetailesButtons.appendChild(computerEditElement)
    computerDetailesButtons.appendChild(computerDeleteElement)

    //item container
    const newComputerRowElement = document.createElement('div');
    newComputerRowElement.classList.add("computer__item");
    //add to item container
    newComputerRowElement.appendChild(computerDetailesContent)
    newComputerRowElement.appendChild(computerDetailesButtons)
    //add item to items container
    const computersContainerElement = document.getElementById("computers__container");
    computersContainerElement.appendChild(newComputerRowElement)

    //add events
    addEventsToComputerItem(computerDetailesButtons, computerDetailes.id)
}

const addEventToAddComputerButton=()=>{

    const addButton=document.getElementById("add__button");

    addButton.addEventListener('click',async()=>{
        const ownerInput=document.getElementById("owner__input");
        const ipInput=document.getElementById("ip__input");
        if (!ownerInput || !ipInput)
            alert("Invalid!")
        else{
            await addComputer(ownerInput.value,ipInput.value);
            await renderComputers();
            ownerInput.value="";
            ipInput.value="";
        }
    })


}

const cleanComputersContainer=()=>{
    const computersContainerElement = document.getElementById("computers__container");
    computersContainerElement.innerHTML="";
}



//main
addEventToAddComputerButton()
const renderComputers=async()=>{
    cleanComputersContainer();
    const allComputers=await getComputersData();
    console.log(allComputers)
    for (let computer of allComputers){
        appendNewComputerRow(computer)
    }
}
renderComputers()